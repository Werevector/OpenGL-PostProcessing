#version 440
layout(location = 0) in vec2 position;
uniform sampler2D fbo_texture;
out vec2 f_texcoord;

void main(void) {
  gl_Position = vec4(position, 0.0, 1.0);
  f_texcoord = (position + 1.0) / 2.0;
}