#version 440
uniform sampler2D fbo_texture;
in vec2 f_texcoord;
out vec4 fcolor;
void main(void) {
  float yd = 400;
  float xd = 600;
  vec4 top         = texture(fbo_texture, vec2(f_texcoord.x, f_texcoord.y + 1.0 / yd));
  vec4 bottom      = texture(fbo_texture, vec2(f_texcoord.x, f_texcoord.y - 1.0 / yd));
  vec4 left        = texture(fbo_texture, vec2(f_texcoord.x - 1.0 / xd, f_texcoord.y));
  vec4 right       = texture(fbo_texture, vec2(f_texcoord.x + 1.0 / xd, f_texcoord.y));
  vec4 topLeft     = texture(fbo_texture, vec2(f_texcoord.x - 1.0 / xd, f_texcoord.y + 1.0 / yd));
  vec4 topRight    = texture(fbo_texture, vec2(f_texcoord.x + 1.0 / xd, f_texcoord.y + 1.0 / yd));
  vec4 bottomLeft  = texture(fbo_texture, vec2(f_texcoord.x - 1.0 / xd, f_texcoord.y - 1.0 / yd));
  vec4 bottomRight = texture(fbo_texture, vec2(f_texcoord.x + 1.0 / xd, f_texcoord.y - 1.0 / yd));
  
  vec4 sx = -topLeft - 2 * left - bottomLeft + topRight   + 2 * right  + bottomRight;
  vec4 sy = -topLeft - 2 * top  - topRight   + bottomLeft + 2 * bottom + bottomRight;
  vec4 sobel = sqrt(sx * sx + sy * sy);
  fcolor = sobel;
}