#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
class Node
{
public:
	Node();
	~Node();

	bool BHasParent();
	bool BHasChild();
	
	void Translate(glm::mat4 t);
	void Rotate(glm::quat r);


private:

	Node* mParent;
	std::vector<Node*> mChildren;

	glm::mat4 mTranslation;
	glm::quat mRotation;

	glm::mat4 mWorldMatrix;
};

