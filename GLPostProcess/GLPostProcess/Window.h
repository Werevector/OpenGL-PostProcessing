#pragma once
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <iostream>

class Window
{
public:
	Window();
	~Window();
	bool Init(int width, int height, std::string name);
	void GetFrameBufferSize(int& width, int& height);

private:
	GLFWwindow* mWindow;
	GLFWwindow* initGLFW(int width, int height, std::string name);
	void InitViewport();
};

