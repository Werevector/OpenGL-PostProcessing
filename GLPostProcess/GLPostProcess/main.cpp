#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <time.h>

#include "tiny_obj_loader.h"
#include "Program.h"
#include "Shader.h"
#include "Window.h"

void initGL()
{
	glewInit();
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
}

void initBuffers(tinyobj::shape_t* shape)
{
	GLuint elems;
	GLuint vao;
	GLuint vbo;
	GLuint nbo;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &elems);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elems);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, shape->mesh.indices.size() * sizeof(unsigned int), &(shape->mesh.indices[0]), GL_DYNAMIC_DRAW);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, shape->mesh.positions.size() * sizeof(float), &(shape->mesh.positions[0]), GL_DYNAMIC_DRAW);

	glGenBuffers(1, &nbo);
	glBindBuffer(GL_ARRAY_BUFFER, nbo);
	glBufferData(GL_ARRAY_BUFFER, shape->mesh.normals.size() * sizeof(float), &(shape->mesh.normals[0]), GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, nbo);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
}

int main()
{
	int width = 640; int height = 480;
	GLfloat fbo_vertices[] = {
		-1, -1,
		1, -1,
		-1,  1,
		1,  1,
	};
	Window window;
	window.Init(width, height, "Post Process");
	initGL();

	//Load model
	//std::string inputfile = "streetscene/street.obj";
	std::string inputfile = "cube.obj";
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string err;
	tinyobj::LoadObj(shapes, materials, err, inputfile.c_str());
	if (err.empty()) { // `err` may contain warning message.
		std::cout << inputfile << " loaded without error \n" << std::endl;
	}
	else {
		std::cout << err;
	}

	//Load model END

	glm::vec3 cpos = glm::vec3(1.0f, 1.0f, 2.0f);
	glm::mat4 view;
	view = glm::lookAt(cpos,
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 projection = glm::perspective(45.0f, (float)width / (float)height, 0.01f, 1000.0f);
	glm::mat4 model = glm::mat4();
	glm::mat4 normalMatrix = glm::mat4();

	std::string shaderPath = "shaders/";
	Program shaderProgram;
	Program pp_shaderProgram;
	Shader vshader(shaderPath + "vertexShader.glsl", GL_VERTEX_SHADER);
	Shader fshader(shaderPath + "fragmentShader.glsl", GL_FRAGMENT_SHADER);
	Shader pp_vshader(shaderPath + "pp_vshader.glsl", GL_VERTEX_SHADER);
	Shader pp_fshader(shaderPath + "pp_fsobel.glsl", GL_FRAGMENT_SHADER);
	shaderProgram.attachShader(&vshader);
	shaderProgram.attachShader(&fshader);
	shaderProgram.linkProgram();
	pp_shaderProgram.attachShader(&pp_vshader);
	pp_shaderProgram.attachShader(&pp_fshader);
	pp_shaderProgram.linkProgram();
	
	shaderProgram.use();

	GLuint ppvao;
	GLuint ppvbo;

	for each (auto var in shapes)
	{
		initBuffers(&var);
	}

	//pp surface
	glGenBuffers(1, &ppvbo);
	glBindBuffer(GL_ARRAY_BUFFER, ppvbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fbo_vertices), fbo_vertices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &ppvao);
	glBindVertexArray(ppvao);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, ppvbo);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	GLuint ModelMatrixHandle = glGetUniformLocation(shaderProgram.getID(), "ModelMatrix");
	GLuint ViewMatrixHandle = glGetUniformLocation(shaderProgram.getID(), "ViewMatrix");
	GLuint ProjectionMatrixHandle = glGetUniformLocation(shaderProgram.getID(), "ProjectionMatrix");
	GLuint NormalMatrixHandle = glGetUniformLocation(shaderProgram.getID(), "NormalMatrix");
	GLuint widthHandle = glGetUniformLocation(pp_shaderProgram.getID(), "width");
	GLuint heightHandle = glGetUniformLocation(pp_shaderProgram.getID(), "height");


	glUniformMatrix4fv(ModelMatrixHandle, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(ViewMatrixHandle, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(ProjectionMatrixHandle, 1, GL_FALSE, glm::value_ptr(projection));
	normalMatrix = glm::inverseTranspose(view*model);
	glUniformMatrix4fv(NormalMatrixHandle, 1, GL_FALSE, glm::value_ptr(normalMatrix));


	GLuint frameBuffer;
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	//glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LESS);
	//glActiveTexture(GL_TEXTURE0);
	GLuint texColorBuffer;
	glGenTextures(1, &texColorBuffer);
	glBindTexture(GL_TEXTURE_2D, texColorBuffer);

	glTexImage2D(
		GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL
	);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texColorBuffer, 0);

	GLuint rboDepthStencil;
	glGenRenderbuffers(1, &rboDepthStencil);
	glBindRenderbuffer(GL_RENDERBUFFER, rboDepthStencil);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);

	glFramebufferRenderbuffer(
		GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rboDepthStencil
	);
	
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	float currentTime = ((float)clock()) / CLOCKS_PER_SEC;
	float delta;
	float nTime;
	//Main App Loop
	while (!glfwWindowShouldClose(window))
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		nTime = ((float)clock()) / CLOCKS_PER_SEC;
		delta = (nTime - currentTime);
		currentTime = nTime;
		model = glm::rotate(model, 0.5f * delta, glm::vec3(0, 1, 0));
		
		shaderProgram.use();
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		glUniformMatrix4fv(ModelMatrixHandle, 1, GL_FALSE, glm::value_ptr(model));
		normalMatrix = glm::inverseTranspose(view*model);
		glUniformMatrix4fv(NormalMatrixHandle, 1, GL_FALSE, glm::value_ptr(normalMatrix));
		for (size_t i = 0; i < g_object_count; i++)
		{
			//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GVaoList[i]);
			glBindVertexArray(GVaoList[i]);
			glDrawElements(GL_TRIANGLES, shapes[i].mesh.indices.size(), GL_UNSIGNED_INT, (void*)0);
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		pp_shaderProgram.use();
		glUniform1i(widthHandle, width);
		glUniform1i(heightHandle, height);

		glBindVertexArray(ppvao);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	
	glDeleteFramebuffers(1, &frameBuffer);
	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}