#include "GLRenderer.h"



GLRenderer::GLRenderer()
{
}


GLRenderer::~GLRenderer()
{
}

void GLRenderer::Init()
{
	glewInit();
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
}
