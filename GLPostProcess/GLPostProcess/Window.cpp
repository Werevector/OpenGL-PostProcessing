#include "Window.h"

Window::Window()
{
}


Window::~Window()
{
}

GLFWwindow* Window::initGLFW(int width, int height, std::string name)
{
	GLFWwindow* window;
	if (!glfwInit())
		return nullptr;

	window = glfwCreateWindow(width, height, name.c_str(), NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return nullptr;
	}

	glfwMakeContextCurrent(window);
	InitViewport();
	return window;
}

void Window::InitViewport()
{
	int width, height;
	GetFrameBufferSize(width, height);
	glViewport(0, 0, width, height);
	glfwSwapInterval(1);
}

bool Window::Init(int width, int height, std::string name)
{
	mWindow = initGLFW(width, height, name);
	if (mWindow == nullptr)
		return false;
}

void Window::GetFrameBufferSize(int & width, int & height)
{
	if(mWindow != nullptr)
		glfwGetFramebufferSize(mWindow, &width, &height);

}
